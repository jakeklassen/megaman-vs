extends KinematicBody2D

var hidden = true

func _ready():
    # Called every time the node is added to the scene.
    # Initialization here
    pass

func _process(delta):
    pass


func _on_RevealTimer_timeout():
    if hidden:
        hidden = false
        $AnimationPlayer.play("reveal")
    else:
        hidden = true
        $AnimationPlayer.play_backwards("reveal")