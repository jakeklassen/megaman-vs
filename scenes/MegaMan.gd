extends KinematicBody2D

var shooting = false

func _ready():
    # Called every time the node is added to the scene.
    # Initialization here
    $AnimationPlayer.play("Run")

func _process(delta):
    # Called every frame. Delta is time since last frame.
    # Update game logic here.
    pass


func _on_Timer_timeout():
    shooting = !shooting
    if shooting: $AnimationPlayer.play("Run Shooting")
    else: $AnimationPlayer.play("Run")
