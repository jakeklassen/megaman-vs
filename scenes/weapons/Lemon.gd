extends Area2D

export (int) var speed
var direction = Vector2(-1, 0)

func _ready():
    # Called every time the node is added to the scene.
    # Initialization here
    pass

func _process(delta):
    # Called every frame. Delta is time since last frame.
    # Update game logic here.
    position += direction * speed * delta


func _on_VisibilityNotifier2D_screen_exited():
    queue_free()


func _on_Area2D_body_entered( body ):
    print(body)
