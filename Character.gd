extends KinematicBody2D

# JV = 250
# G = 350

export (PackedScene) var Lemon

signal shoot

const MOVE_SPEED = 120

enum State { IDLE, AIRBORNE, RUNNING }

var jump_height = 52
var time_to_jump_apex = 0.3
var jump_velocity
var gravity
var input = Vector2()
var velocity = Vector2()
var direction = -1
var shooting = false
var can_shoot = true
var current_state = {
    grounded = false,
    moving = false
}
var last_state = {
    grounded = false,
    moving = false
}

func _ready():
    gravity = (2 * jump_height) / pow(time_to_jump_apex, 2)
    jump_velocity = -(abs(gravity) * time_to_jump_apex)
    $RunningShootingAnimation.visible = false
    $AnimatedSprite.visible = true

func _process(delta):
    current_state.grounded = is_on_floor()
    current_state.moving = Input.is_action_pressed("ui_right") || Input.is_action_pressed("ui_left")

    $RunningShootingAnimation.visible = false

    if is_on_ceiling() || is_on_floor():
        velocity.y = gravity * delta

    if Input.is_action_pressed("ui_right"):
        input.x = 1
        direction = 1
    elif Input.is_action_pressed("ui_left"):
        input.x = -1
        direction = -1
    else:
        input.x = 0

    if Input.is_action_just_pressed("shoot") && can_shoot:
        shooting = true
        can_shoot = false
        $ShootingPoseTimer.start()
        $ShotDelayTimer.start()
        var lemon = Lemon.instance()
        get_parent().add_child(lemon)
        var offset = $BulletOffset.position if is_on_floor() else $JumpBulletOffset.position
        lemon.position = position + offset * (Vector2(-1, 1) if direction == 1 else Vector2(1, 1))
        # Add player velocity as well
        lemon.position += velocity * delta
        lemon.direction.x = direction

    $AnimatedSprite.flip_h = true if direction == 1 else false
    $RunningShootingAnimation.flip_h = $AnimatedSprite.flip_h

    if is_on_floor() && Input.is_action_just_pressed("jump"):
        velocity.y = jump_velocity

    if input.x != 0:
        if is_on_floor():
            if $ShootingPoseTimer.get_time_left() > 0:
                var frame = $AnimatedSprite.frame
                var animation = $AnimatedSprite.animation
                if $AnimatedSprite.animation != "run_shoot": $AnimatedSprite.play("run_shoot")
                if animation == "run": $AnimatedSprite.frame = frame
            else:
                var frame = $AnimatedSprite.frame
                var animation = $AnimatedSprite.animation
                if $AnimatedSprite.animation != "run": $AnimatedSprite.play("run")
                if animation == "run_shoot": $AnimatedSprite.frame = frame

        else:
            if shooting: $AnimatedSprite.play("jump_shoot")
            else: $AnimatedSprite.play("jump")
    else:
        if is_on_floor():
            if shooting: $AnimatedSprite.play("shoot")
            else: $AnimatedSprite.play("idle")
        else:
            if shooting: $AnimatedSprite.play("jump_shoot")
            else: $AnimatedSprite.play("jump")

#    if is_on_floor():
#        if input.x != 0: $AnimatedSprite.play("run")
#        else:
#            if shooting: $AnimatedSprite.play("shoot")
#            else: $AnimatedSprite.play("idle")
#    else: $AnimatedSprite.play("jump")

    velocity.y += gravity * delta
    velocity.x = MOVE_SPEED * input.x
    var motion = move_and_slide(velocity, Vector2(0, -1))

    last_state.grounded = current_state.grounded
    last_state.moving = current_state.moving

    update()

func _draw():
    # draw_line(Vector2(0, 0), Vector2(15, 0), Color(1, 0, 0), 1)
    var extents = $CollisionShape2D.shape.extents
    var shape_pos = $CollisionShape2D.position
#    draw_rect(
#        Rect2(
#            Vector2(-extents.x + shape_pos.x, -extents.y + shape_pos.y),
#            extents * 2
#        ),
#        Color(1, 1, 0, 0.25),
#        true
#    )

#func _physics_process(delta):
#    var space_state = get_world_2d().get_direct_space_state()
#    # use global coordinates, not local to node
#    var result = space_state.intersect_ray(position, Vector2(position.x, position.y + 35), [self])
#
#    if (not result.empty()):
#        # collision
#        pass

func _on_player_shoot():
    pass


func _on_ShootingPoseTimer_timeout():
    shooting = false


func _on_ShotDelayTimer_timeout():
    can_shoot = true
